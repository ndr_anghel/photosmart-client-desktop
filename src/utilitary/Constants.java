package utilitary;

public class Constants {

	private static final String FXML_PATH = "/fxml/";

	public static final String START_PAGE_FXML = FXML_PATH + "StartPage.fxml";

	public static final String APPLICATION_NAME = "Image Gallery";

	public static final String IMAGE_GALLERY_FXML = FXML_PATH + "ImageGallery.fxml";

}
