package controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javafx.beans.value.ChangeListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.TilePane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import utilitary.Constants;

public class ImageGalleryController {

	private Stage applicationStage;
	private Scene applicationScene;

	@FXML
	private TilePane tilePane;
	@FXML
	private MenuBar menuBar;
	@FXML
	private Menu menuFile;
	@FXML
	private Menu menuEdit;
	@FXML
	private ScrollPane scrollPane;
	@FXML
	private ColorPicker colorPicker;
	@FXML
	private TextField startTSTextField;
	@FXML
	private TextField endTSTextField;
	@FXML
	private ComboBox<String> comboBox;

	public void start(String user) {

		try {
			applicationScene = new Scene(
					(Parent) FXMLLoader.load(getClass().getResource(Constants.IMAGE_GALLERY_FXML)));

		} catch (Exception exception) {
			System.out.println("An exception has occured: " + exception.getMessage());
			exception.printStackTrace();
		}

		applicationStage = new Stage();
		applicationStage.setTitle(Constants.APPLICATION_NAME);
		// applicationStage.getIcons().add(new
		// Image(getClass().getResource(Constants.ICON_FILE_NAME).toExternalForm()));
		applicationStage.setScene(applicationScene);

		applicationStage.show();
	}

	@FXML
	private void initialize() {

		comboBox.getItems().addAll("By color", "By Timestamp");
		comboBox.setEditable(true);
		comboBox.valueProperty().addListener((ChangeListener<String>) (ov, t, t1) -> {
			// depending on the value, do the search here
		});

		String path = "/home/arh6/Pictures";

		File folder = new File(path);
		File[] listOfFiles = folder.listFiles();

		if (listOfFiles == null) {
			System.out.println("folder e null");
		}

		for (final File file : listOfFiles) {
			if (file.isDirectory()) {
				continue;
				// FIXME or just make a recursive method. blabla////
			}

			System.out.println("file name: " + file.getName());
			ImageView imageView;
			imageView = createImageView(file);
			if (imageView != null) {
				tilePane.getChildren().add(imageView);
			}
		}
	}

	private ImageView createImageView(final File imageFile) {
		// DEFAULT_THUMBNAIL_WIDTH is a constant you need to define
		// The last two arguments are: preserveRatio, and use smooth (slower)
		// resizing

		ImageView imageView = null;
		try {
			final Image image = new Image(new FileInputStream(imageFile), 150, 0, true, true);
			imageView = new ImageView(image);
			imageView.setFitWidth(150);
			imageView.setOnMouseClicked(new EventHandler<MouseEvent>() {

				@Override
				public void handle(MouseEvent mouseEvent) {

					if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {

						if (mouseEvent.getClickCount() == 2) {
							try {
								BorderPane borderPane = new BorderPane();
								ImageView imageView = new ImageView();
								Image image = new Image(new FileInputStream(imageFile));
								imageView.setImage(image);
								imageView.setStyle("-fx-background-color: BLACK");
								imageView.setFitHeight(scrollPane.getHeight() - 10);
								imageView.setPreserveRatio(true);
								imageView.setSmooth(true);
								imageView.setCache(true);
								borderPane.setCenter(imageView);
								borderPane.setStyle("-fx-background-color: BLACK");
								Stage newStage = new Stage();
								newStage.setWidth(scrollPane.getWidth());
								newStage.setHeight(scrollPane.getHeight());
								newStage.setTitle(imageFile.getName());
								Scene scene = new Scene(borderPane, Color.BLACK);
								newStage.setScene(scene);
								newStage.show();

								scene.setOnMouseClicked(new EventHandler<MouseEvent>() {

									@Override
									public void handle(MouseEvent event) {
										// ((Scene)
										// event.getSource()).getWindow().hide();

									}
								});
							} catch (FileNotFoundException e) {
								e.printStackTrace();
							}

						}
					}
				}
			});
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		}
		return imageView;
	}

	@FXML
	public void startTimestampKeyPressedEvent(KeyEvent event) {
		// id event is enter. store the ts.
	}

	@FXML
	public void endTimestampKeyPressedEvent(KeyEvent event) {
	}

	@FXML
	public void choseColorFromColorPickerAction(ActionEvent event) {
		System.out.println("the picked color is: " + colorPicker.getValue());
	}


}
